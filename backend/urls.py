from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView
from django.contrib import admin

from . import views

app_name = 'backend'
urlpatterns = [
    path('', views.admin, name='admin'),

    path('categories/', views.category_list, name='category_list'),
    path('categories/new/', views.category_new, name='category_new'),
    path('categories/add/', views.category_add, name='category_add'),
    path('categories/<int:category_id>/', views.category_details, name='category_details'),
    path('categories/<int:category_id>/edit/', views.category_edit, name='category_edit'),
    path('categories/<int:category_id>/delete/', views.category_delete, name='category_delete'),

    path('e/', views.event_list, name='event_list'),
    path('e/new/', views.event_new, name='event_new'),
    path('e/add/', views.event_add, name='event_add'),
    path('e/<int:event_id>/dashboard', views.event_dashboard, name='event_dashboard'),
    path('e/<int:event_id>/publish', views.event_publish_button, name='event_publish_button'),
    path('e/<int:event_id>/', views.event_details, name='event_details'),
    path('e/<int:event_id>/edit/', views.event_edit, name='event_edit'),
    path('e/<int:event_id>/delete/', views.event_delete, name='event_delete'),

    path('e/<int:event_id>/t/', views.ticket_list, name='ticket_list'),
    path('e/<int:event_id>/t/new/', views.ticket_new, name='ticket_new'),
    path('e/<int:event_id>/t/add/', views.ticket_add, name='ticket_add'),
    path('e/<int:event_id>/t/<int:ticket_id>/', views.ticket_details, name='ticket_details'),
    path('e/<int:event_id>/t/<int:ticket_id>/edit/', views.ticket_edit, name='ticket_edit'),
    path('e/<int:event_id>/t/<int:ticket_id>/delete/', views.ticket_delete, name='ticket_delete'),

    path('e/<int:event_id>/attendees/', views.attendee_list, name='attendee_list'),
    path('e/<int:event_id>/attendees/download/', views.attendee_download, name='attendee_download'),
    path('e/<int:event_id>/attendees/download-addons/', views.attendee_download_addon, name='attendee_download_addon'),
    path('e/<int:event_id>/orders/', views.order_list, name='order_list'),
    path('e/<int:event_id>/orders/download/', views.order_download, name='order_download'),
    path('e/<int:event_id>/stats/', views.stat_show, name='stat_show'),
    path('e/<int:event_id>/stats/location-download', views.stat_location_download, name='stat_location_download'),
    path('e/<int:event_id>/stats/date-ticket-download', views.stat_date_ticket_download, name='stat_date_ticket_download'),
    path('e/<int:event_id>/stats/date-addon-download', views.stat_date_addon_download, name='stat_date_addon_download'),
    path('e/<int:event_id>/stats/revenue-download', views.stat_revenue_download, name='stat_revenue_download'),
]
