import csv, datetime, json

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.template import loader
from django.db.models import Sum, Count
from django.db.models.functions import TruncDay
from datetime import datetime, timedelta, date
from django.contrib import messages

from users.models import User

from backend.models import *
from backend.logic.stat import *
from backend.logic.get_models import *
from backend.logic.dashboard import *


@login_required
def admin(request):
    TODAY = date.today()

    upcoming_events_list = Event.objects.filter(user_id=request.user.id, date_end__gte=TODAY).order_by('date_start')[:6]
    recent_orders_list = get_recent_orders_from_all_users_events(request.user.id, 5)

    user = get_object_or_404(User, pk=request.user.id)
    #print(user.org_identifier)
    context = {
        'upcoming_events_list': upcoming_events_list,
        'recent_orders_list': recent_orders_list,
        'user': user,
    }
    return render(request, 'backend/admin.html', context)

@login_required
def category_list(request):
    all_categories_list_with_count = get_categories_list_with_count(request.user.id)

    context = {
        'all_categories_list_with_count': all_categories_list_with_count,
    }
    return render(request, 'backend/category/list.html', context)

@login_required
def category_new(request):
    form = CategoryForm()
    return render(request, 'backend/category/new.html', {'form': form})

@login_required
@require_http_methods(["POST"])
def category_add(request):
    form = CategoryForm(request.POST)

    if form.is_valid():
        category = form.save(commit=False)
        user = User.objects.get(id=request.user.id)
        category.user = user
        category.save()
        return HttpResponseRedirect(reverse('backend:category_list'))

@login_required
def category_details(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    form = CategoryForm(instance=category)
    context = {
        'form': form,
        'category': category
    }
    return render(request, 'backend/category/details.html', context)

@login_required
@require_http_methods(["POST"])
def category_edit(request, category_id):
    category = get_object_or_404(Category, pk=category_id)

    form = CategoryForm(request.POST, instance=category)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('backend:category_list'))
    else:
        return render(request, 'backend/category/details.html', {'form': form, 'category': category})

def category_delete(request, category_id):
    Category.objects.get(id=category_id).delete()
    return HttpResponseRedirect(reverse('backend:category_list'))
    

@login_required
def event_list(request):
    all_events_list = Event.objects.filter(user_id=request.user.id).order_by('date_start')
    context = {
        'all_events_list': all_events_list,
    }
    return render(request, 'backend/event/list.html', context)

def event_dashboard(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    context = get_dashboard_context(request, event)
    return render(request, 'backend/event/dashboard.html', context)

def event_publish_button(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    event.is_published = True
    event.save()
    context = get_dashboard_context(request, event)
    return render(request, 'backend/event/dashboard.html', context)

@login_required
def event_new(request):
    form = EventFormCreate()
    # only displays categories created by current user
    form.fields["category"].queryset = Category.objects.filter(user_id=request.user.id)

    return render(request, 'backend/event/new.html', {'form': form})

@login_required
@require_http_methods(["POST"])
def event_add(request):
    form = EventFormCreate(request.POST, request.FILES)

    if form.is_valid():
        event = form.save(commit=False)
        user = User.objects.get(id=request.user.id)
        event.user = user
        event.save()
        return HttpResponseRedirect(reverse('backend:event_dashboard', kwargs={'event_id': event.id}))
    else:
        return render(request, 'backend/event/new.html', {'form': form})

@login_required
def event_details(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    form = EventFormEdit(instance=event)
    # only displays categories created by current user
    form.fields["category"].queryset = Category.objects.filter(user_id=request.user.id)
    
    context = {
        'form': form,
        'event': event
    }
    return render(request, 'backend/event/details.html', context)

@login_required
@require_http_methods(["POST"])
def event_edit(request, event_id):
    event = get_object_or_404(Event, pk=event_id)

    form = EventFormEdit(request.POST, request.FILES, instance=event)

    # Save edited event
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('backend:event_dashboard', kwargs={'event_id': event.id}))
    else:
        return render(request, 'backend/event/details.html', {'form': form, 'event': event})

def event_delete(request, event_id):
    all_tickets_list_with_count = get_tickets_list_with_count(event_id)
    
    can_delete_event = True
    for ticket in all_tickets_list_with_count:
        print(str(ticket.id) + " " + str(ticket.count))
        if ticket.count > 0:
            can_delete_event = False
    
    if can_delete_event:
        Event.objects.filter(id=event_id).first().delete()
        messages.add_message(request, messages.SUCCESS,  \
            'Event successfully deleted.'  \
        )
    else:
        messages.add_message(request, messages.ERROR,  \
            'You cannot delete an event when at least one ticket has been sold. Make the event a "draft" instead.'  \
        )

    return HttpResponseRedirect(reverse('backend:event_list'))


@login_required
def ticket_list(request, event_id):
    all_tickets_list_with_count = get_tickets_list_with_count(event_id)

    event = get_object_or_404(Event, pk=event_id)
    context = {
        'all_tickets_list_with_count': all_tickets_list_with_count,
        'event': event,
    }

    return render(request, 'backend/ticket/list.html', context)

@login_required
def ticket_new(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    form = TicketForm()
    context = {
        'form': form,
        'event': event
    }
    return render(request, 'backend/ticket/new.html', context)

@login_required
@require_http_methods(["POST"])
def ticket_add(request, event_id):
    form = TicketForm(request.POST)

    if form.is_valid():
        ticket = form.save(commit=False)
        event = Event.objects.get(id=event_id)
        ticket.event = event
        ticket.save()
        return HttpResponseRedirect(reverse('backend:ticket_list', kwargs={'event_id': event.id}))
    else:
        return render(request, 'backend/ticket/new.html', {'form': form})

@login_required
def ticket_details(request, event_id, ticket_id):
    ticket = get_object_or_404(Ticket, pk=ticket_id)
    event = get_object_or_404(Event, pk=event_id)
    form = TicketForm(instance=ticket)
    context = {
        'form': form,
        'ticket': ticket,
        'event': event
    }
    return render(request, 'backend/ticket/details.html', context)

@login_required
@require_http_methods(["POST"])
def ticket_edit(request, event_id, ticket_id):
    ticket = get_object_or_404(Ticket, pk=ticket_id)
    event = get_object_or_404(Event, pk=event_id)

    form = TicketForm(request.POST, instance=ticket)

    if form.is_valid():
        print('is valid')
        form.save()
        return HttpResponseRedirect(reverse('backend:ticket_list', kwargs={'event_id': event.id}))
    else:
        print('is NOT valid')
        return render(request, 'backend/ticket/details.html', {'form': form, 'ticket': ticket, 'event': event})

def ticket_delete(request, event_id, ticket_id):
    ticket_with_count = get_ticket_with_count(ticket_id)

    if ticket_with_count.count > 0:
        messages.add_message(request, messages.ERROR,  \
            'You cannot delete a ticket when at least one has been sold. Hide the ticket and change the "for sale end date" instead.'  \
        )
    else:
        ticket_with_count.delete()
        messages.add_message(request, messages.SUCCESS,  \
            'Ticket successfully deleted.'  \
        )

    return HttpResponseRedirect(reverse('backend:ticket_list', kwargs={'event_id': event_id}))



@login_required
def attendee_list(request, event_id):
    all_attendees_list = TicketOrder.objects.select_related('ticket').filter(ticket__event__id=event_id, ticket__ticket_type=Ticket.TicketType.TICKET)
    event = get_object_or_404(Event, pk=event_id)
    context = {
        'all_attendees_list': all_attendees_list,
        'event': event,
    }
    return render(request, 'backend/attendee/list.html', context)

@login_required
def attendee_download(request, event_id):
    all_attendees_list = TicketOrder        \
        .objects.select_related('ticket')   \
        .filter(ticket__event__id=event_id, ticket__ticket_type=Ticket.TicketType.TICKET) \
        .order_by('last_name')
    
    file_name = "attendees_list_" + datetime.now().strftime('%H-%M-%S')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + file_name + '.csv"'

    template = loader.get_template('backend/attendee/download_template.txt')
    c = {'data': all_attendees_list}
    response.write(template.render(c))

    return response

@login_required
def attendee_download_addon(request, event_id):
    all_addons_list = TicketOrder        \
        .objects.select_related('ticket')   \
        .filter(ticket__event__id=event_id, ticket__ticket_type=Ticket.TicketType.EXTRA) \
        .order_by('order__email')
    
    file_name = "addons_list_" + datetime.now().strftime('%H-%M-%S')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + file_name + '.csv"'

    template = loader.get_template('backend/attendee/download_template_addon.txt')
    c = {'data': all_addons_list}
    response.write(template.render(c))

    return response

@login_required
def order_list(request, event_id):
    all_orders_list = Order.objects.filter(event_id=event_id).distinct().order_by('-date')
    event = get_object_or_404(Event, pk=event_id)
    context = {
        'all_orders_list': all_orders_list,
        'event': event,
    }
    return render(request, 'backend/order/list.html', context)

@login_required
def order_download(request, event_id):
    all_orders_list = Order.objects.filter(event_id=event_id).distinct().order_by('-date')

    file_name = "orders_list_" + datetime.now().strftime('%H-%M-%S')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + file_name + '.csv"'

    template = loader.get_template('backend/order/download_template.txt')
    c = {'data': all_orders_list}
    response.write(template.render(c))

    return response

@login_required
def stat_show(request, event_id):
    #all_orders = Order.objects.filter(event_id=event_id).distinct().order_by('-date')

    # tickets sales by country
    group_by_country = Order.objects  \
        .filter(event_id=event_id, ticketorder__ticket__ticket_type=Ticket.TicketType.TICKET)  \
        .values('country')  \
        .annotate(count=Count('ticketorder'))  \
        .values('count', 'country')
    
    location_data = list(group_by_country)

    # ticket sales by date
    DAYS_INTO_PAST = 14
    TODAY = date.today()
    many_days_ago = TODAY - timedelta(days=DAYS_INTO_PAST)
    group_by_date_tickets = Order.objects  \
        .filter(event_id=event_id, date__gte=many_days_ago, ticketorder__ticket__ticket_type=Ticket.TicketType.TICKET)  \
        .values(date_no_time=TruncDay('date'))  \
        .annotate(count=Count('ticketorder'))  \
        .values('count', 'date_no_time')
    
    date_ticket_data = get_stat_location_data_json(group_by_date_tickets, TODAY, DAYS_INTO_PAST)

    group_by_date_addon = Order.objects  \
        .filter(event_id=event_id, date__gte=many_days_ago, ticketorder__ticket__ticket_type=Ticket.TicketType.EXTRA)  \
        .values(date_no_time=TruncDay('date'))  \
        .annotate(count=Count('ticketorder'))  \
        .values('count', 'date_no_time')
    
    date_addon_data = get_stat_location_data_json(group_by_date_addon, TODAY, DAYS_INTO_PAST)

    group_by_date_revenue = Order.objects  \
        .filter(event_id=event_id)  \
        .values(date_no_time=TruncDay('date'))  \
        .values('date_no_time')  \
        .annotate(sum=Sum('order_total'))  \
        .values('sum', 'date_no_time')

    revenue_data = get_stat_revenue_data_json(group_by_date_revenue, TODAY, DAYS_INTO_PAST)

    event = get_object_or_404(Event, pk=event_id)
    context = {
        'event': event,
        'location_data': location_data,
        'date_ticket_data': date_ticket_data,
        'date_addon_data': date_addon_data,
        'revenue_data': revenue_data,
    }
    return render(request, 'backend/stat/show.html', context)

@login_required
def stat_location_download(request, event_id):
    all_orders = Order.objects  \
        .filter(event_id=event_id, ticketorder__ticket__ticket_type=Ticket.TicketType.TICKET)  \
        .distinct()  \
        .order_by('-date')

    file_name = "stats_location_" + datetime.now().strftime('%H-%M-%S')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + file_name + '.csv"'

    template = loader.get_template('backend/stat/download_template_location.txt')
    c = {'data': all_orders}
    response.write(template.render(c))

    return response

@login_required
def stat_date_ticket_download(request, event_id):
    group_by_date = Order.objects  \
        .filter(event_id=event_id, ticketorder__ticket__ticket_type=Ticket.TicketType.TICKET)  \
        .values(date_no_time=TruncDay('date'))  \
        .annotate(count=Count('ticketorder'))  \
        .values('count', 'date_no_time')

    file_name = "stats_tickets_sold_per_day_" + datetime.now().strftime('%H-%M-%S')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + file_name + '.csv"'

    template = loader.get_template('backend/stat/download_template_date_ticket.txt')
    c = {'data': group_by_date}
    response.write(template.render(c))

    return response

@login_required
def stat_date_addon_download(request, event_id):
    group_by_date = Order.objects  \
        .filter(event_id=event_id, ticketorder__ticket__ticket_type=Ticket.TicketType.EXTRA)  \
        .values(date_no_time=TruncDay('date'))  \
        .annotate(count=Count('ticketorder'))  \
        .values('count', 'date_no_time')

    file_name = "stats_tickets_sold_per_day_" + datetime.now().strftime('%H-%M-%S')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + file_name + '.csv"'

    template = loader.get_template('backend/stat/download_template_date_addon.txt')
    c = {'data': group_by_date}
    response.write(template.render(c))

    return response

@login_required
def stat_revenue_download(request, event_id):
    group_by_date_revenue = Order.objects  \
        .filter(event_id=event_id)  \
        .values(date_no_time=TruncDay('date'))  \
        .values('date_no_time')  \
        .annotate(sum=Sum('order_total'))  \
        .values('sum', 'date_no_time')

    file_name = "stats_revenue_" + datetime.now().strftime('%H-%M-%S')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + file_name + '.csv"'

    template = loader.get_template('backend/stat/download_template_revenue.txt')
    c = {'data': group_by_date_revenue}
    response.write(template.render(c))

    return response
