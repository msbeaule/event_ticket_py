from django.db.models import Count, Sum

from backend.models import Ticket, Order, Category

def get_tickets_list_with_count(event_id):
    return Ticket.objects.filter(event_id=event_id).annotate(count=Count('ticketorder__ticket__id')).order_by('for_sale_date_start')

def get_ticket_with_count(ticket_id):
    return Ticket.objects.filter(id=ticket_id).annotate(count=Count('ticketorder__ticket__id')).first()

def get_categories_list_with_count(user_id):
    return Category.objects.filter(user_id=user_id).annotate(count=Count('event__id')).order_by('name')

def get_recent_orders_from_all_users_events(user_id, number_of_orders):
    return Order.objects.filter(event__user_id=user_id).order_by('-date')[:number_of_orders]

def get_recent_orders(user_id, event_id, number_of_orders):
    return Order.objects.filter(event__user_id=user_id, event_id=event_id).order_by('-date')[:number_of_orders]

def get_gross_revenue(user_id, event_id):
    revenue = Order.objects.filter(event__user_id=user_id, event_id=event_id).aggregate(r=Sum('order_total'))['r']
    if revenue is None:
        revenue = 0.00
    return revenue
