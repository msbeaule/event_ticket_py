from django.shortcuts import get_object_or_404

from django.db.models import Count
from django.db.models.functions import TruncDay
from datetime import datetime, timedelta, date

from backend.models import Event
from backend.logic.stat import get_stat_location_data_json
from backend.logic.get_models import *

def get_dashboard_context(request, event):
    all_tickets_list_with_count = get_tickets_list_with_count(event.id)
    recent_orders_list = get_recent_orders(request.user.id, event.id, 5)

    gross_revenue = get_gross_revenue(request.user.id, event.id)

    DAYS_INTO_PAST = 7
    TODAY = date.today()
    many_days_ago = TODAY - timedelta(days=DAYS_INTO_PAST)
    group_by_date_tickets = Order.objects  \
        .filter(event_id=event.id, date__gte=many_days_ago, ticketorder__ticket__ticket_type=Ticket.TicketType.TICKET)  \
        .values(date_no_time=TruncDay('date'))  \
        .annotate(count=Count('ticketorder'))  \
        .values('count', 'date_no_time')
    
    date_ticket_data = get_stat_location_data_json(group_by_date_tickets, TODAY, DAYS_INTO_PAST)

    context = {
        'event': event,
        'all_tickets_list_with_count': all_tickets_list_with_count,
        'recent_orders_list': recent_orders_list,
        'date_ticket_data': date_ticket_data,
        'gross_revenue': gross_revenue,
    }
    return context
