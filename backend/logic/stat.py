import json
from datetime import timedelta
from operator import itemgetter

def get_stat_location_data_json(group_by_date, TODAY, DAYS_INTO_PAST):

    group_by_date = list(group_by_date)

    # must take the data and manually make it into json, as both serializer and json.dumps() both
    # don't work when used on a QuerySet when .annotate() and .values() are used on it
    tickets_sold_per_date = []
    for i in group_by_date:
        json_obj = dict([
            ('count', i['count']),
            ('date_no_time', i['date_no_time'].strftime('%Y-%m-%d')),
        ])
        tickets_sold_per_date.append(json_obj)

    date_data = tickets_sold_per_date.copy()

    # adds in the days (in the last 14 days) where no tickets where sold,
    # so the JavaScript chart is displayed nicely
    for x in range(0, DAYS_INTO_PAST):
        date_obj = TODAY - timedelta(days=x)
        date_str = date_obj.strftime('%Y-%m-%d')

        does_it_exist = next((i for i in tickets_sold_per_date if i["date_no_time"] == date_str), False)
        if does_it_exist is False:
            json_obj = dict([
                ('count', 0),
                ('date_no_time', date_str),
            ])
            date_data.append(json_obj)

    date_data = sorted(date_data, key=itemgetter('date_no_time'))
    return json.dumps(date_data, default=str)

def get_stat_revenue_data_json(group_by_date_revenue, TODAY, DAYS_INTO_PAST):

    group_by_date_revenue = list(group_by_date_revenue)

    # must take the data and manually make it into json, as both serializer and json.dumps() both
    # don't work when used on a QuerySet when .annotate() and .values() are used on it
    revenue_per_date = []
    for i in group_by_date_revenue:
        json_obj = dict([
            ('sum', i['sum']),
            ('date_no_time', i['date_no_time'].strftime('%Y-%m-%d')),
        ])
        revenue_per_date.append(json_obj)

    for item in revenue_per_date:
        print(item['sum'])
        print(item['date_no_time'])

    revenue_data = revenue_per_date.copy()

    # adds in the days (in the last 14 days) where no tickets where sold,
    # so the JavaScript chart is displayed nicely
    for x in range(0, DAYS_INTO_PAST):
        date_obj = TODAY - timedelta(days=x)
        date_str = date_obj.strftime('%Y-%m-%d')

        does_it_exist = next((i for i in revenue_per_date if i["date_no_time"] == date_str), False)
        if does_it_exist is False:
            json_obj = dict([
                ('sum', 0),
                ('date_no_time', date_str),
            ])
            revenue_data.append(json_obj)

    revenue_data = sorted(revenue_data, key=itemgetter('date_no_time'))
    return json.dumps(revenue_data, default=str)
