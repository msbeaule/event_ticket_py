path_length = 60

var name_field = document.getElementsByName('name')[0]
name_field.addEventListener(
    'focusout',
    function() {
        copy_to_path(this);
    },
    false
);

function copy_to_path(name_field) {
    var path_field = document.getElementsByName('path')[0]
    if (path_field.value.trim() == "") {
        cleaned_name = name_field.value.trim().toLowerCase()
        
        // replace spaces with a dash
        cleaned_name = cleaned_name.replace(/[ ]{1,}/g, '-')
        
        // all characters that have no business being in a slug.
        // django still validates in the backend, but
        // this helps create a slug from the event name
        cleaned_name = cleaned_name.replace(/[\/{}|\\^\[\]`:;\?@&\*=\+,\'\"\!<>()#]/g, '')

        if (cleaned_name.length > path_length) {
            cleaned_name = cleaned_name.substring(0, path_length)
        }

        path_field.value = cleaned_name
        console.log('something?')
    }
}
