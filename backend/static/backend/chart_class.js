class MyChart {
    constructor(element_id, type, data, label, labels) {
        this.element_id = element_id
        this.type = type
        this.data = data
        this.label = label
        this.labels = labels
        this.options
        this.background_color
        this.border_color
        this.chart

        this.__set_options()
        this.__set_background_and_border_color()
        this.__make_chart()
    }

    get_ctx() {
        return document.getElementById(this.element_id);
    }

    __make_chart() {
        this.chart = new Chart(this.get_ctx(), {
            type: this.type,
            data: {
                labels: this.labels,
                datasets: [{
                    label: this.label,
                    data: this.data,
                    backgroundColor: this.background_color,
                    borderColor: this.border_color,
                    borderWidth: 1
                }]
            },
            options: this.options
        });
    }

    __set_options() {
        switch(this.type) {
            case 'pie':
                this.options = {
                    responsive: true,
                    // maintainAspectRatio: false
                }
            break;
            case 'bar':
                this.options = {
                    responsive: true,
                    // maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            break;
        }
    }

    __set_background_and_border_color() {
        switch(this.type) {
            case 'pie':
                this.background_color = [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ]
                this.border_color = [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ]
            break;
            case 'bar':
                this.background_color = 'rgba(54, 162, 235, 0.2)'
                this.border_color = 'rgba(54, 162, 235, 1)'
            break;
        }
    }

    get_chart() {
        return this.chart
    }
}
