from django.core.management.base import BaseCommand

from users.models import User

from backend.models import *

class Command(BaseCommand):
    def handle(self, **options):
        text = input("Enter your company / organization (default is 'test'):")
        if text.strip() is "":
            text = "test"
        user = User.objects.get(id=1)
        user.org_name = text

        path = text.lower()
        path = re.sub('[^a-zA-Z0-9_-]', '', path)
        if path is "":
            path = "test"
        user.org_identifier = path
        user.save()
