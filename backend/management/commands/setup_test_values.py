from django.core.management.base import BaseCommand
from datetime import datetime, timedelta
from django.utils.timezone import make_aware

from users.models import User

from backend.models import *

class Command(BaseCommand):
    def handle(self, **options):
        date_start = datetime.strptime("2020/09/22 08:00:00", "%Y/%m/%d %H:%M:%S")
        date_end = datetime.strptime("2020/09/23 17:00:00", "%Y/%m/%d %H:%M:%S")

        # converting into local timezone
        date_start = make_aware(date_start)
        date_end = make_aware(date_end)

        user = User.objects.get(id=1)
        category = Category.objects.create(name="Demo Category", path="democategory", user=user)
        
        event = Event.objects.create(name="Demo Event", path="demo-event3", description="This is just a demo event.",   \
            venue_name="Filberg Heritage Lodge and Park", venue_address="Comox, BC", date_start=date_start,             \
            date_end=date_end, is_published=True, is_hidden=False, category=category, refunds=Event.Refunds.NO_REFUNDS, \
            user=user)

        now = datetime.now()
        now = make_aware(now)
        two_days_ago = now - timedelta(days=2)

        ticket_one = Ticket.objects.create(name="Early Bird", path="eb", description="A test ticket.",   \
            for_sale_date_start=now, for_sale_date_end=date_start, ticket_type=Ticket.TicketType.TICKET,    \
            quantity=25, price=50, event=event)

        ticket_two = Ticket.objects.create(name="General Admission", path="ga", description="A test ticket.",   \
            for_sale_date_start=now, for_sale_date_end=date_start, ticket_type=Ticket.TicketType.TICKET,    \
            quantity=50, price=60, event=event)

        addon_one = Ticket.objects.create(name="Drink Add-on", path="add-on", description="A drink add-on for this event.",   \
            for_sale_date_start=now, for_sale_date_end=date_start, ticket_type=Ticket.TicketType.EXTRA,    \
            quantity=100, price=20, event=event)

        # --- order one ---

        order_one = Order.objects.create(first_name="Ernest", last_name="Thornhill", email="themachine@example.com",  \
            date=now, address_one="New York", city="New York City", province="New York", country="USA",  \
            order_total="120", event=event, status=Order.Status.COMPLETE)
        
        ticket_order_one = TicketOrder.objects.create(first_name="Harold", last_name="Finch", email="themachine@example.com",  \
            order=order_one, ticket=ticket_two, status=TicketOrder.Status.COMPLETE)

        ticket_order_two = TicketOrder.objects.create(first_name="John", last_name="Reese", email="themachine@example.com",  \
            order=order_one, ticket=ticket_two, status=TicketOrder.Status.COMPLETE)

        # --- order two ---

        order_two = Order.objects.create(first_name="Ernest", last_name="Thornhill", email="themachine@example.com",  \
            address_one="New York", city="New York City", province="New York", country="USA",  \
            order_total="120", event=event, status=Order.Status.COMPLETE)
        Order.objects.filter(pk=order_two.id).update(date=two_days_ago)
        
        ticket_order_three = TicketOrder.objects.create(first_name="Samantha", last_name="Groves", email="themachine@example.com",  \
            order=order_two, ticket=ticket_one, status=TicketOrder.Status.COMPLETE)

        ticket_order_four = TicketOrder.objects.create(first_name="Sameen", last_name="Shaw", email="themachine@example.com",  \
            order=order_two, ticket=ticket_one, status=TicketOrder.Status.COMPLETE)
        
        addon_order_one = TicketOrder.objects.create(order=order_two, ticket=addon_one, status=TicketOrder.Status.COMPLETE)

        addon_order_two = TicketOrder.objects.create(order=order_two, ticket=addon_one, status=TicketOrder.Status.COMPLETE)
