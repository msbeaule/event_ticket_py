from django.contrib import admin
from .models import Event, Category, Ticket, Order, TicketOrder, Cart

admin.site.register(Event)
admin.site.register(Category)
admin.site.register(Ticket)
admin.site.register(Order)
admin.site.register(TicketOrder)

admin.site.register(Cart)
