from django.db import models
from django.forms import ModelForm
from django.contrib.postgres.fields import *
from event_ticket_py import settings

from psycopg2.extras import DateTimeTZRange
from django.core.validators import *

from backend.validators import *

from django.utils import timezone

from django import forms

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.user.id, filename)

class Category(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False)
    path = models.CharField(max_length=30, null=False, validators=[validate_unicode_slug])
    description = models.TextField(blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    class Meta:
        verbose_name_plural = "categories"
    def __str__(self):
        return self.name

class Event(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False)
    path = models.CharField(max_length=60, null=False, validators=[validate_unicode_slug])
    description = models.TextField(blank=True, null=True)
    venue_name = models.CharField(max_length=100, blank=False, null=False)
    venue_address = models.CharField(max_length=100, blank=False, null=False)
    class Refunds(models.IntegerChoices):
        ONE_DAY = 1
        ONE_WEEK = 2
        ONE_MONTH = 3
        NO_REFUNDS = 4
        NOT_DEFINED = 5
    refunds = models.IntegerField(choices=Refunds.choices, default=Refunds.NOT_DEFINED, null=False)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    is_published = models.BooleanField(default=False)
    is_hidden = models.BooleanField(default=False)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    banner = models.ImageField(upload_to=user_directory_path, blank=True, null=True, validators=[file_size])

    class Meta:
        unique_together = ['path', 'user']

    def __str__(self):
        return self.name

class Ticket(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False)
    path = models.CharField(max_length=60, null=False, validators=[validate_unicode_slug])
    description = models.TextField(blank=True, null=True)
    for_sale_date_start = models.DateTimeField(blank=False, null=False)
    for_sale_date_end = models.DateTimeField(blank=False, null=False)
    is_hidden = models.BooleanField(default=False)
    class TicketType(models.IntegerChoices):
        TICKET = 1
        EXTRA = 2
    ticket_type = models.IntegerField(choices=TicketType.choices, blank=False)
    quantity = models.IntegerField(blank=False, null=False)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['path', 'event']
    
    def __str__(self):
        return "(id: " + str(self.id) + ") " + self.name
    
class Order(models.Model):
    first_name = models.CharField(max_length=200, blank=False, null=False)
    last_name = models.CharField(max_length=200, blank=False, null=False)
    email = models.EmailField(blank=False, null=False)
    date = models.DateTimeField(auto_now_add=True)
    credit_last_four = models.CharField(max_length=4, default="0003", blank=False, null=False, validators=[MinLengthValidator(4), MaxLengthValidator(4)])
    address_one = models.CharField(max_length=200, blank=False, null=False)
    address_two = models.CharField(max_length=200, blank=True, null=True)
    city = models.CharField(max_length=200, blank=False, null=False)
    province = models.CharField(max_length=200, blank=False, null=False)
    country = models.CharField(max_length=200, blank=False, null=False)
    order_total = models.DecimalField(max_digits=10, decimal_places=2)
    refunded_amount = models.DecimalField(max_digits=9, decimal_places=2, default=0, blank=True, null=False)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=True, null=False)
    class Status(models.IntegerChoices):
        PENDING = 1
        COMPLETE = 2
        REFUNDED = 3
    status = models.IntegerField(choices=Status.choices, blank=False)
    def __str__(self):
        return self.first_name + " ($" + str(self.order_total) + ")" + " - " + self.date.strftime('%Y-%m-%d %H:%M') + " UTC"

class TicketOrder(models.Model):
    first_name = models.CharField(max_length=200, blank=False, null=True)
    last_name = models.CharField(max_length=200, blank=False, null=True)
    email = models.EmailField(blank=False, null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, blank=True, null=False)
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, blank=True, null=False, related_name='ticketorder')
    class Status(models.IntegerChoices):
        PENDING = 1
        COMPLETE = 2
        REFUNDED = 3
    status = models.IntegerField(choices=Status.choices, blank=False)
    def __str__(self):
        ticket_name = self.ticket.name
        return str(self.order.id) + " " + self.order.first_name + " " + self.order.last_name + " - " + ticket_name

class Cart(models.Model):
    code = models.CharField(max_length=36)
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, blank=False, null=False)
    quantity = models.IntegerField(blank=False, null=False, default=0)
    price = models.IntegerField(blank=False, null=False, default=0)
    def __str__(self):
        return str(self.code) + " - " + self.ticket.name + " / " + self.ticket.event.name

class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'path', 'description']

class EventFormCreate(ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'path', 'description', 'banner', 'venue_name', 'venue_address', 'refunds', 'is_hidden', 'date_start', 'date_end', 'category']
        labels = {
            'is_hidden': 'Should event be hidden?'
        }
        widgets = {
            'date_start': forms.TextInput(attrs={'class': "date-picker"}),
            'date_end': forms.TextInput(attrs={'class': "date-picker"}),
        }

class EventFormEdit(ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'path', 'description', 'banner', 'venue_name', 'venue_address', 'refunds', 'date_start', 'date_end', 'is_hidden', 'category', 'is_published']
        labels = {
            'is_hidden': 'Should event be hidden?'
        }
        widgets = {
            'date_start': forms.TextInput(attrs={'class': "date-picker"}),
            'date_end': forms.TextInput(attrs={'class': "date-picker"}),
        }

class TicketForm(ModelForm):
    class Meta:
        model = Ticket
        fields = ['name', 'path', 'description', 'for_sale_date_start', 'for_sale_date_end', 'is_hidden', 'ticket_type', 'quantity', 'price']
        labels = {
            'date': 'Date displayed on ticket'
        }
        widgets = {
            'for_sale_date_start': forms.TextInput(attrs={'class': "date-picker"}),
            'for_sale_date_end': forms.TextInput(attrs={'class': "date-picker"}),
        }

class TicketOrderForm(ModelForm):
    class Meta:
        model = TicketOrder
        fields = ['first_name', 'last_name', 'email', 'ticket']
        widgets = {
            'ticket': forms.HiddenInput(),
        }

class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'address_one', 'address_two', 'city', 'province', 'country']
