var ticket_data_counts = []
var ticket_data_dates = []

for (var i = 0; i < date_ticket_data.length; i++) {
    ticket_data_counts.push(date_ticket_data[i].count)
    ticket_data_dates.push(date_ticket_data[i].date_no_time)
}

var date_ticket_chart = new MyChart('chart_date_ticket', 'bar', ticket_data_counts, 'Tickets Sold Per Day', ticket_data_dates)
