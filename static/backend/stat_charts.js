// ---------- LOCATION DATA CHART ----------

var location_data_quantities = []
var location_data_countries = []

for (var i = 0; i < location_data.length; i++) {
    location_data_quantities.push(location_data[i].count)
    location_data_countries.push(location_data[i].country)
}

var location_chart = new MyChart('chart_location', 'pie', location_data_quantities, '# of attendees per country', location_data_countries)


// ---------- TICKETS BOUGHT PER DAY DATA CHART ----------

var ticket_data_counts = []
var ticket_data_dates = []

for (var i = 0; i < date_ticket_data.length; i++) {
    ticket_data_counts.push(date_ticket_data[i].count)
    ticket_data_dates.push(date_ticket_data[i].date_no_time)
}

var date_ticket_chart = new MyChart('chart_date_ticket', 'bar', ticket_data_counts, 'Tickets Sold Per Day', ticket_data_dates)


// ---------- ADD-ONS BOUGHT PER DAY DATA CHART ----------

var addon_data_counts = []
var addon_data_dates = []

for (var i = 0; i < date_addon_data.length; i++) {
    addon_data_counts.push(date_addon_data[i].count)
    addon_data_dates.push(date_addon_data[i].date_no_time)
}

var date_addon_chart = new MyChart('chart_date_addon', 'bar', addon_data_counts, 'Add-Ons Sold Per Day', addon_data_dates)


// ---------- GROSS REVENUE PER DAY DATA CHART ----------

var revenue_data_sum = []
var revenue_data_dates = []

for (var i = 0; i < revenue_data.length; i++) {
    revenue_data_sum.push(revenue_data[i].sum)
    revenue_data_dates.push(revenue_data[i].date_no_time)
}

var date_addon_chart = new MyChart('chart_revenue', 'bar', revenue_data_sum, 'Revenue Per Day', revenue_data_dates)
