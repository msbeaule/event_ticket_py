var options = {
    enableTime: true,
    dateFormat: "Y-m-d H:i",
}

flatpickr(".date-picker", options);
