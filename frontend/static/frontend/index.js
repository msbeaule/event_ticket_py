var i = 0;
var txt = 'The simple event ticket application.';
var speed = 100;

function clear_text() {
    get_tagline_text_element().innerHTML = "";
}

function type_writer() {
    if (i < txt.length) {
        get_tagline_text_element().innerHTML += txt.charAt(i);
        i++;
        setTimeout(type_writer, speed);
    }
}

function get_tagline_text_element() {
    return document.getElementById("tagline").getElementsByClassName("text")[0]
}

clear_text()
type_writer()
