var form = document.getElementById('order_form')
var tables = form.getElementsByTagName('table')
var copy_form_div = null;
var heading_names = null;


if (HTMLCollection.prototype.isPrototypeOf(tables) && tables.length) {
    for (i = 0; i < tables.length; i++) {
        table = tables[i]
        // console.log(table)
    }

    // doesn't include ticket buyer
    copy_form_div = document.getElementsByClassName('copy-form')
    // includes ticket buyer
    heading_names = document.getElementsByClassName('heading-name')

    for (var i = 0; i < copy_form_div.length; i++) {
        var this_forms_value = parseInt(tables[i+1].getAttribute('data-number'))

        console.log(this_forms_value)

        var selectList = document.createElement("select")
        selectList.setAttribute('data-number', this_forms_value)
        selectList.classList.add('copy-form-from')
        selectList.addEventListener(
            'change',
            function() {
                option_selected(this);
            },
            false
         );
        copy_form_div[i].appendChild(selectList)

        var option = document.createElement("option")
        option.text = 'Copy info from...'
        option.selected = true
        option.hidden = true
        selectList.appendChild(option)

        for (var z = 0; z < heading_names.length; z++) {

            // doesn't need to display itself in the dropdown
            if (z !== this_forms_value) {
                option = document.createElement("option")
                option.value = tables[z].getAttribute('data-number')
                option.text = heading_names[z].innerText
                selectList.appendChild(option)
            }
        }
    }

}

function option_selected(that) {
    var current_form_value = that.getAttribute('data-number')
    var selected_value = that.options[that.selectedIndex].value;

    copy_data(selected_value, current_form_value)
}

function copy_data(fields_copy_from, fields_copy_to) {
    fields_copy_from = tables[fields_copy_from].querySelectorAll("input:not([type=hidden])")
    fields_copy_to = tables[fields_copy_to].querySelectorAll("input:not([type=hidden])")

    for (i = 0; i < fields_copy_to.length; i++) {
        fields_copy_to[i].value = fields_copy_from[i].value
    }
}
