from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views.generic.edit import CreateView
from users.forms import CustomUserCreationForm
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from datetime import datetime
from django.db.models import Sum, Count

from users.models import User
from backend.models import *
from frontend.forms import *
from frontend.logic.general import *
from frontend.logic.order import *

def index(request):
    context = {}
    return render(request, 'frontend/index.html', context)

class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('frontend:login')
    template_name = 'frontend/signup.html'

def org(request, org_path):
    event_user = get_event_user_from_org_path(org_path)
    all_events_list = Event.objects.filter(user_id=event_user.id).order_by('date_start')
    context = {
        'all_events_list': all_events_list,
        'event_user': event_user,
    }
    print (all_events_list)
    return render(request, 'frontend/org.html', context)

def event(request, org_path, event_id):
    event_user = get_event_user_from_org_path(org_path)
    event = get_object_or_404(Event, pk=event_id)

    TODAY = datetime.today()

    all_tickets_list = Ticket.objects.filter(event_id=event.id, is_hidden=False, for_sale_date_start__lte=TODAY, for_sale_date_end__gte=TODAY, ticket_type=Ticket.TicketType.TICKET).order_by('for_sale_date_end')
    all_extras_list = Ticket.objects.filter(event_id=event.id, is_hidden=False, for_sale_date_start__lte=TODAY, for_sale_date_end__gte=TODAY, ticket_type=Ticket.TicketType.EXTRA).order_by('for_sale_date_end')

    form = NumberOfTicketsToOrderForm(tickets=all_tickets_list)
    form_extra = NumberOfExtrasToOrderForm(extras=all_extras_list)

    context = {
        'event': event,
        'event_user': event_user,
        'all_tickets_list': all_tickets_list,
        'all_extras_list': all_extras_list,
        'form': form,
        'form_extra': form_extra,
    }
    return render(request, 'frontend/event.html', context)


def order_new(request, org_path, event_id):
    # checks if any tickets or add-ons were selected. If not, doesn't let
    # the user load the checkout page (order_new)
    ids = get_list_of_ticket_ids_quantity(request)
    if not ids:
        print("in here")
        return HttpResponseRedirect(reverse('frontend:event', args=(org_path, event_id)))
    
    event_user = get_event_user_from_org_path(org_path)
    event = get_object_or_404(Event, pk=event_id)

    session_key = request.session.session_key

    if not request.session.exists(session_key):
        request.session.create()
        session_key = request.session.session_key

    if request.method == 'POST':
        clear_cart(session_key)

    save_ids_and_quantities_to_cart(session_key, ids)

    all_cart_list = get_cart(session_key)

    ticket_forms, ticket_prefixes, ticket_list = get_ticket_forms_prefixes_and_list(ids, event_id)

    order_form = OrderForm(prefix="order")

    total_price = get_cart_total_price(all_cart_list)

    context = {
        'event': event,
        'event_user': event_user,
        'order_form': order_form,
        'ticket_forms': ticket_forms,
        'ticket_prefixes': ticket_prefixes,
        'all_cart_list': all_cart_list,
        'ticket_list': ticket_list,
        'total_price': total_price,
    }
    return render(request, 'frontend/order_new.html', context)


@require_http_methods(["POST"])
def order_add(request, org_path, event_id):
    event_user = get_event_user_from_org_path(org_path)
    event = get_object_or_404(Event, pk=event_id)

    all_cart_list = get_cart(request.session.session_key)

    order_form = OrderForm(request.POST, prefix="order")

    cart_prefixes = get_cart_prefixes(all_cart_list)

    ticket_forms = list()

    prefixes = request.POST.get('ticket_prefixes').split(',')

    print(str(prefixes) + "\t" + str(cart_prefixes))

    # must be sorted, or the following if statement might fail
    cart_prefixes.sort()
    prefixes.sort()

    # check if the data from POST matches the data from the database
    if cart_prefixes == prefixes:
    
        for i in prefixes:
            ticket_forms.append(TicketOrderForm(request.POST, prefix=i))

        if order_form.is_valid() and are_tickets_valid(ticket_forms):
            order = order_form.save(commit=False)

            order.order_total = get_cart_total_price(all_cart_list)
            order.status = Order.Status.COMPLETE
            order.event = event

            order.save()

            # saves the tickets and the info from the form
            for form in ticket_forms:
                form = form.save(commit=False)
                form.order = order
                form.status = TicketOrder.Status.COMPLETE
                form.save()
            
            # saves the extras/add ons
            for cart in all_cart_list:
                if cart.ticket.ticket_type == Ticket.TicketType.EXTRA:
                    for q in range(0, cart.quantity):
                        TicketOrder.objects.create(ticket=cart.ticket, status=TicketOrder.Status.COMPLETE, order=order)

            clear_cart(request.session.session_key)
            save_order_id(request, order.id)

            return HttpResponseRedirect(reverse('frontend:order_thanks', args=(event_user.org_identifier, event.id)))

        else:
            clear_cart(request.session.session_key)
            context = {
                'event': event,
                'event_user': event_user,
                'order_form': order_form,
                'ticket_forms': ticket_forms,
                'all_cart_list': all_cart_list
            }
            return render(request, 'frontend/order_new.html', context)
    else:
        all_tickets_list = Ticket.objects.filter(event_id=event.id, is_hidden=False).order_by('name')
        form = NumberOfTicketsToOrderForm(tickets=all_tickets_list)
        context = {
            'event': event,
            'event_user': event_user,
            'all_tickets_list': all_tickets_list,
            'form': form,

        }
        messages.add_message(request, messages.ERROR, 'Something went wrong. Please try again.')
        return HttpResponseRedirect(reverse('frontend:event', args=(event_user.org_identifier, event.id)))

def order_thanks(request, org_path, event_id):
    order_id = get_order_id(request)

    event_user = get_event_user_from_org_path(org_path)
    event = get_object_or_404(Event, pk=event_id)
    order = get_object_or_404(Order, pk=order_id)
    count = TicketOrder.objects.filter(order__id=order_id)  \
        .values('ticket')                                   \
        .annotate(Count('ticket'))                          \
        .values('ticket', 'ticket__count', 'ticket__name')

    context = {
        'event': event,
        'event_user': event_user,
        'order': order,
        'count': count,
    }
    return render(request, 'frontend/order_thanks.html', context)
