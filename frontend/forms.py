from django import forms

class NumberOfTicketsToOrderForm(forms.Form):
   
    def __init__(self, *args, **kwargs):
        tickets = kwargs.pop('tickets')
        super(NumberOfTicketsToOrderForm, self).__init__(*args, **kwargs)

        for i, ticket in enumerate(tickets):
            self.fields['ticket_%s' % ticket.id] = forms.IntegerField(label=ticket.name, required=False, initial=0)

class NumberOfExtrasToOrderForm(forms.Form):
   
    def __init__(self, *args, **kwargs):
        extras = kwargs.pop('extras')
        super(NumberOfExtrasToOrderForm, self).__init__(*args, **kwargs)

        for i, extra in enumerate(extras):
            self.fields['extra_%s' % extra.id] = forms.IntegerField(label=extra.name, required=False, initial=0)
