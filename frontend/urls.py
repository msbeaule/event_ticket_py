from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView
from django.contrib import admin

from . import views
from frontend.views import SignUpView

app_name = 'frontend'
urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', views.index, name='index'),
    path('o/<str:org_path>/', views.org, name='org'),
    path('o/<str:org_path>/<int:event_id>/', views.event, name='event'),
    path('o/<str:org_path>/<int:event_id>/order/', views.order_new, name='order_new'),
    path('o/<str:org_path>/<int:event_id>/order/add/', views.order_add, name='order_add'),
    path('o/<str:org_path>/<int:event_id>/order/confirmation/', views.order_thanks, name='order_thanks'),
]
