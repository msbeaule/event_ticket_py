from django.shortcuts import get_object_or_404
from django.db.models import Sum
from backend.models import Cart, Ticket, TicketOrderForm

def are_tickets_valid(ticket_list):
    for item in ticket_list:
        if item.is_valid() is not True:
            return False
    return True

def clear_cart(session_key):
    carts = Cart.objects.filter(code=session_key)
    if carts.exists():
        carts.delete()

def get_cart(session_key):
    return Cart.objects.filter(code=session_key).order_by('ticket__id')

def get_cart_total_price(cart):
    return cart.aggregate(Sum('price'))['price__sum']

def save_order_id(request, order_id):
    request.session['order_id'] = order_id

def get_order_id(request):
    return request.session['order_id']

def save_ids_and_quantities_to_cart(session_key, ticket_ids_and_quantities):
    for ticket_id, quantity in ticket_ids_and_quantities.items():
        ticket = get_object_or_404(Ticket, pk=ticket_id)
        price = ticket.price * quantity
        cart = Cart(code=session_key, ticket=ticket, quantity=quantity, price=price)
        cart.save()

def get_list_of_ticket_ids_quantity(request):
    ids = dict()
    for i in request.POST.lists():
        #print(i)
        if i[0].startswith("ticket_") or i[0].startswith("extra_"):
            ticket_id = int(i[0].split('_')[1])
            if Ticket.objects.filter(id=ticket_id).exists():
                try:
                    quantity = int(i[1][0])
                except ValueError:
                    quantity = 0
                if quantity > 0:
                    if i[0].startswith("ticket_") or i[0].startswith("extra_"):
                        ids[ticket_id] = quantity

    return ids


def get_ticket_forms_prefixes_and_list(ids, event_id):
    ticket_forms = list()
    ticket_prefixes = list()
    ticket_list = list()

    ids_from_database = list(Ticket.objects.filter(event_id=event_id, ticket_type=Ticket.TicketType.TICKET).values_list('id', flat=True))

    print(ids_from_database)

    for key, quantity in ids.items():
        # only want forms to be created for the tickets, not the addons/extras
        if key in ids_from_database:
            for i in range(0, quantity):
                prefix = str(key) + "_" + str(i)
                ticket_prefixes.append(prefix)
                form = TicketOrderForm(prefix=prefix, initial={'ticket': key})
                ticket_forms.append(form)
                ticket_list.append(get_object_or_404(Ticket, pk=key))

    ticket_prefixes = ','.join(ticket_prefixes)

    return ticket_forms, ticket_prefixes, ticket_list

def get_cart_prefixes(all_cart_list):
    cart_prefixes = list()
    for cart in all_cart_list:
        for q in range(0, cart.quantity):
            if cart.ticket.ticket_type == Ticket.TicketType.TICKET:
                cart_prefixes.append(str(cart.ticket.id) + "_" + str(q))
    return cart_prefixes
