from users.models import User
from django.shortcuts import get_object_or_404

def get_event_user_from_org_path(org_path):
    event_user = get_object_or_404(User, org_identifier=org_path)
    return event_user
