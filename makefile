requirements:
	pip install -r requirements.txt

drop_db:
	echo 'DROP DATABASE eventticket' | psql -U postgres

create_db:
	echo 'CREATE DATABASE eventticket' | psql -U postgres

migrate:
	python manage.py makemigrations
	python manage.py migrate

superuser:
	python manage.py createsuperuser

reset: drop_db create_db migrate

reset_full: drop_db create_db migrate superuser setup

run:
	python manage.py runserver 0.0.0.0:8000

setup:
	python manage.py set_org
	python manage.py setup_test_values
