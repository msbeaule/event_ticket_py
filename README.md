# Eventori

## Installation

Please read [the installation guide](https://gitlab.com/msbeaule/event_ticket_py/-/wikis/Installation) to learn how to install and run this project.

## About

Learn more about this project [here](https://gitlab.com/msbeaule/event_ticket_py/-/wikis/About).
